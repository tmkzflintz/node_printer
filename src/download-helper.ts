import fs = require("fs");
import url = require("url");
import http = require("http");
import https = require("https");
import mkdirp = require("mkdirp");

export function downloadFile(
  urlFile: string,
  options: any,
  callback: any
) {
  if (!callback && typeof options === "function") {
    callback = options;
  }
  console.log("====================HERE===================");

  options = typeof options === "object" ? options : {};
  options.timeout = options.timeout || 20000;
  options.directory = options.directory ? options.directory : ".";
  options.filename = options.filename;

  const path = options.directory + "/" + options.filename;

  let req: any;
  if (url.parse(urlFile).protocol === "https:") {
    req = https;
  } else {
    req = http;
  }

  const request = req
    .get(urlFile, response => {
      if (response.statusCode === 200) {
        mkdirp(options.directory, err => {
          if (err) {
            throw err;
          }
          // tslint:disable-next-line:no-shadowed-variable
          const file = fs.createWriteStream(path);
          response.pipe(file);
        });
      } else {
        if (callback) {
          callback(response.statusCode);
        }
      }

      response.on("end", () => {
        if (callback) {
          callback(false, path);
        }
      });

      request.setTimeout(options.timeout, () => {
        request.abort();
        callback("Timeout");
      });
    })
    .on("error", e => {
      if (callback) {
        callback(e);
      }
    });

  return request;
}
