import moment = require("moment");
import printer = require("node-native-printer");
import { downloadFile } from "./download-helper";
import { Download } from "./downloader";
import Axios from "axios";

async function download(url) {
  const resPath = await Download.download(url);
  return resPath;
}

let receiptArr = new Array();
Axios.create()
  .get(
    "http://127.0.0.1:3000/receipt/printing-copy-receipt/get-printing-receipt"
  )
  .then(res => {
    console.log(res.data);
  })
  .catch();

console.log(printer.listPrinters());
