import path = require("path");
import moment = require("moment");
import { downloadFile } from "./download-helper";
export class Download {
  static download(url: string): Promise<any> {
    const filename = path.basename(url);
    const dir = moment().format("YYYYMMDD");
    const options = {
      directory: `./src/download/${dir}`,
      filename: filename
    };

    return new Promise((resolve, reject) => {
      downloadFile(url, options, err => {
        if (err) {
          console.log(`=====================> download error `);
          console.log(err);
        } else {
          console.log(`=====================> download success `);
          resolve(`src/download/${dir}/${filename}`);
        }
      });
    });
  }
}
